import React, { useState, useEffect } from "react";

export default function ProductDetailForm(props) {
  const [title, setTitle] = useState("");
  const [detail, setDetail] = useState("");
  const [stock, setStock] = useState(0);
  const [price, setPrice] = useState(0);

  useEffect(() => {
    setTitle(props.product.title);
    setDetail(props.product.detail);
    setStock(props.product.stock);
    setPrice(props.product.price);
  }, []);

  const handleEditProduct = (e) => {
    e.preventDefault();
    let dataEdit = {
      user_id: localStorage.getItem("user_id"),
      title: title,
      detail: detail,
      stock: stock,
      price: price,
    };
    props.updateProduct(dataEdit);
  };

  return (
    <div className="container mt-5">
      <div class="card mb-3">
          <div class="row no-gutters">
            <div class="col-md-4">
                <img src={ process.env.PUBLIC_URL + 'assets/images/1.jpg'} class="card-img" alt="..."/>
            </div>
            <div class="col-md-8">
              <div class="card-body p-5">
                  <h5 class="card-title">ชื่อสินค้า : {title}</h5>
                  <p class="card-text">รายละเอียดสินค้า : {detail}</p>
                  <p>ราคา : {price}</p>
                  <p>สินค้าคงเหลือ : {stock}</p>
              </div>
                {/* <Link to="/cart">
                    <button type="submit" class="btn btn-primary mb-2">หยิบใส่ตระกร้า</button>
                </Link> */}
                <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
            </div>
          </div>
        </div>
    </div>


  );
}
