import React, { useState } from "react";
import { Link } from "react-router-dom";

export default function TableProducts(props) {

  return (
    <div className="container">
      <div class="row">       
       {props.products
            .filter((item) => {
              return (
                item.title ||
                item.detail ||
                item.stock  ||
                item.price 
              );
            })
            .map((item, index) => (
        <div class="col-sm-6">

          <div class="card mt-3">
          
            <div class="card-body">
              <h5 class="card-title">{item.title}</h5>
              {/* <p class="card-text">{item.detail}</p> */}
              {/* <p class="card-text">{item.stock}</p> */}
              <p class="card-text">ราคา : {item.price}</p>
              <Link class="btn btn-primary"
                    to={`/productdetail/${item._id}`}
              >
                    รายละเอียดสินค้า
              </Link>
            </div>
            
          </div>
            
        </div>))}
      </div>
    </div>
  );
}
