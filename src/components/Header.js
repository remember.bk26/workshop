import React, { useState, useEffect } from "react";
import { NavLink, Redirect } from "react-router-dom"
import { isLogout, isLogin } from "../AuthToken/AuthToken"

export default function Header(props) {
  const [navigate, setNavigate] = useState("");

  const goToEditProfile = () => {
    setNavigate("edit-profile");
  };

  const goToLogout = () => {
    isLogout();
    props.setAuth(isLogin());
    setNavigate("logout");
  };
    return (
        <div>
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
          <a class="navbar-brand" href="#">Sneakers Shop</a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav ml-auto">
            <li className={props.auth ? "d-none d-print-block" : "nav-item"}>
              <NavLink
                exact
                to="/login"
                className="nav-link"
                activeClassName="active"
              >
                Login
              </NavLink>
            </li>
            <li className={props.auth ? "d-none d-print-block" : "nav-item"}>
              <NavLink
                exact
                to="/register"
                className="nav-link"
                activeClassName="active"
              >
                Register
              </NavLink>
            </li>
            <li className={props.auth ? "nav-item" : "d-none d-print-block"}>
              <NavLink
                exact
                to="/products"
                className="nav-link"
                activeClassName="active"
              >
                Products
              </NavLink>
            </li>

            <li className={props.auth ? "nav-item" : "d-none d-print-block"}>
              <NavLink
                exact
                to="/profile"
                className="nav-link"
                activeClassName="active"
              >
                Profile
              </NavLink>
            </li>
            <li className={props.auth ? "nav-item" : "d-none d-print-block"}>
              <a
                  className="nav-link"
                  style={{ cursor: "pointer" }}
                  onClick={goToLogout}
                >
                  Logout
                </a>
            </li>
            </ul>
          </div>
        </nav>
    </div>
    )
}
