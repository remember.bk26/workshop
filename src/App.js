import { Route, Switch, Redirect } from "react-router-dom";
import React, { useState } from "react";

import EditProfile from "./pages/EditProfile/EditProfile";
import Header from "./components/Header";
import Login from "./pages/Login/Login";
import PrivateRoute from "./helper/PrivateRoute";
import Products from "./pages/Products/Products";
import Profile from "./pages/Profile/Profile";
import Register from "./pages/Register/Register";
import Cart from "./pages/Cart/Cart";
import NotFound from "./pages/NotFound/NotFound";

import "./App.css";
import { isLogin, getDisplayName } from "./AuthToken/AuthToken";
import ProductDetail from "./pages/ProductDetail/ProductDetail";

var routers = {
  login: "/login",
  register: "/register",
  profile: "/profile",
  products: "/products",
  editprofile: "/edit-profile/:id",
  productdetail: "/productdetail/:id",
  cart: "/cart/:id",
  notfound: "*",
};

function App() {
  const [auth, setAuth] = useState(isLogin());
  const [displayname, setDisplayName] = useState(getDisplayName());
  return (
    <div className="App">
      <Header auth={auth} displayname={displayname} setAuth={setAuth} />
        <Switch>
          <Redirect exact from="/" to={routers.login} />
          <Route
            exact
            path={routers.login}
            component={(props) => (
              <Login
                props={props}
                setAuth={setAuth}
                setDisplayName={setDisplayName}
              />
            )}
          />
          <Route exact path={routers.register} component={Register} />
          <PrivateRoute exact path={routers.products} component={Products} />
          <PrivateRoute exact path={routers.profile} component={Profile} />
          <PrivateRoute exact path={routers.cart} component={Cart} />
          <PrivateRoute exact path={routers.productdetail} component={ProductDetail} />
          <PrivateRoute exact path={routers.editprofile} component={EditProfile}/>
          <Route exact path={routers.notfound} component={NotFound} />
        </Switch>
      </div>
  );
}

export default App;
