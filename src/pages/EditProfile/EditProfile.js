import React, { useState, useEffect } from "react";
import { getUserById, editUserById } from "../../api/api";

export default function EditProfile(props) {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [name, setName] = useState("");
  const [age, setAge] = useState(0);
  const [salary, setSalary] = useState(0);

  const handleEditProfile = async (e) => {
    e.preventDefault();
    let dataUpdate = {
      username: username,
      password: password,
      name: name,
      age: age,
      salary: salary,
    };

    await editUserById(props.match.params.id, dataUpdate).then((res) => {
      if (res.status === "success") {
        props.history.push("/profile");
      }
    });
  };

  const fetchUser = async () => {
    await getUserById(props.match.params.id).then((res) => {
      if (res.status === "success") {
        setUsername(res.data.username);
        setPassword(res.data.password);
        setName(res.data.name);
        setAge(res.data.age);
        setSalary(res.data.salary);
      }
    });
  };
  useEffect(() => {
    fetchUser();
  }, []);

  return (
    <div className="container mt-5">
      <form onSubmit={handleEditProfile}>
        <h1 className="text-warning">แก้ไข ข้อมูลส่วนตัว</h1>
        <div class="form-group row mt-3">
          <label for="staticEmail" class="col-sm-2 col-form-label">Email</label>
          <div class="col-sm-10">
          <input
                    type="text"
                    value={username}
                    className="form-control"
                    id="username"
                    placeholder=""
                    disabled
                  />
          </div>
        </div>
        <div class="form-group row">
          <label for="inputPassword" class="col-sm-2 col-form-label">Password</label>
          <div class="col-sm-10">
          <input
                    type="password"
                    value={password}
                    className="form-control"
                    id="password"
                    placeholder=""
                    disabled
                  />
          </div>
        </div>
        <div class="form-group row">
          <label for="inputPassword" class="col-sm-2 col-form-label">ชื่อ</label>
          <div class="col-sm-10">
          <input
                    type="text"
                    value={name}
                    onChange={(e) => setName(e.target.value)}
                    className="form-control"
                    id="name"
                    placeholder=""
                  />
          </div>
        </div>
        <div class="form-group row">
          <label for="inputPassword" class="col-sm-2 col-form-label">อายุ</label>
          <div class="col-sm-10">
          <input
                    type="number"
                    value={age}
                    onChange={(e) => setAge(e.target.value)}
                    className="form-control"
                    id="age"
                    placeholder=""
                  />
          </div>
        </div>
        <div class="form-group row">
          <label for="inputPassword" class="col-sm-2 col-form-label">เงินเดือน</label>
          <div class="col-sm-10">
          <input
                    type="number"
                    value={salary}
                    onChange={(e) => setSalary(e.target.value)}
                    className="form-control"
                    id="salary"
                    placeholder=""
                  />
          </div>
        </div>
        <button type="submit" className="btn btn-success btn-block">
                  บันทึก
                </button>
      </form>
    </div>
  );
}
