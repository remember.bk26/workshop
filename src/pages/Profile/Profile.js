import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { getUserById } from "../../api/api";

export default function Profile() {
  const [userProfile, setUserProfile] = useState([]);

  useEffect(() => {
    fetchUser();
  }, []);

  const fetchUser = async () => {
    await getUserById(localStorage.getItem("user_id")).then((res) => {
      if (res.status === "success") {
        setUserProfile(res.data);
      }
    });
  };

  return (
    <div className="container mt-5">
      <form>
        <h1 className="text-warning">ข้อมูลส่วนตัว</h1>
        <div class="form-group row mt-3">
          <label for="staticEmail" class="col-sm-2 col-form-label">Email</label>
          <div class="col-sm-10">
            <input type="text" readonly class="form-control-plaintext" id="" value={userProfile.username}/>
          </div>
        </div>
        <div class="form-group row">
          <label for="inputPassword" class="col-sm-2 col-form-label">Password</label>
          <div class="col-sm-10">
            <input type="password" class="form-control" value={userProfile.password}/>
          </div>
        </div>
        <div class="form-group row">
          <label for="inputPassword" class="col-sm-2 col-form-label">ชื่อ</label>
          <div class="col-sm-10">
            <input type="text" class="form-control" value={userProfile.name} readonly/>
          </div>
        </div>
        <div class="form-group row">
          <label for="inputPassword" class="col-sm-2 col-form-label">อายุ</label>
          <div class="col-sm-10">
            <input type="number" class="form-control" value={userProfile.age}/>
          </div>
        </div>
        <div class="form-group row">
          <label for="inputPassword" class="col-sm-2 col-form-label">เงินเดือน</label>
          <div class="col-sm-10">
            <input type="number" class="form-control" value={userProfile.salary}/>
          </div>
        </div>
        <Link
                      
                      to={`/edit-profile/${localStorage.getItem("user_id")}`}
                      className="btn btn-info btn-block"
                    >
                      แก้ไขข้อมูล
                    </Link>
      </form>
    </div>
  );
}
