import React, { useState, useEffect } from "react";

import { getAllProducts } from "../../api/api";
import ProductDetailForm from "../../components/ProductDetailForm";

export default function ProductDetail(props) {
  const [product, setProduct] = useState();

  const fetchProduct = async () => {
    await getAllProducts().then((res) => {
      let data = res.data.filter((item) => {
        return item._id === props.match.params.id;
      });
      setProduct(data[0]);
    });
  };

//   const updateProduct = async (product) => {
//     await editProduct(props.match.params.id, product).then((res)=>{
//         if(res.status === "success"){
//             props.history.push('/products')
//         }
//     })
//   };
  
  useEffect(() => {
    fetchProduct();
  }, []);

  console.log(props.match.params.id);
  return (
    <div>
        <div className="container">
          <h1>รายละเอียดสินค้า</h1>
          {product !== undefined && (
            <ProductDetailForm product={product} />
          )}
      </div>
    </div>
  );
}
