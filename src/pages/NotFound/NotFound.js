import React from "react";


export default function NotFound() {
  return (
        <div class="container">
          <h1>
            <i class="fa fa-map-signs" aria-hidden="true"></i> NotFound
          </h1>
        </div>
  );
}
